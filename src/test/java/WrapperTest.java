import org.assertj.core.api.Assertions;
import org.junit.Test;

public class WrapperTest {

    @Test
    public void testWrap() {
        Assertions.assertThat(Wrapper.wrap("", 1)).isNotNull();
        Assertions.assertThat(Wrapper.wrap("test break", 2)).isEqualTo("test break");
        Assertions.assertThat(Wrapper.wrap("test break", 5)).isEqualTo("test\nbreak");
        Assertions.assertThat(Wrapper.wrap("", 0)).isEqualTo("");
        Assertions.assertThat(Wrapper.wrap("test", -1)).isEqualTo("test");
        Assertions.assertThat(Wrapper.wrap("test break", 6)).isEqualTo("test\nbreak");
    }
}