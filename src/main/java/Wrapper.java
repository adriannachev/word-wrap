public class Wrapper {
    static String wrap(String input, int column) {
        if (input.length() < column) {
            System.out.println("Your column number is bigger than your string size. Column number has been set to string size");
            column = input.length();
        }

        if (column < 0) {
            System.out.println("Column number cannot be a negative number. Column number set to 0");
            column = 0;
        }

        String secondPart = getNonChangeablePart(input, column);
        String firstPart = getChangedFirstPart(input, column);

        return firstPart + secondPart;
    }

    static private String getChangedFirstPart(String input, int column) {
        String firstPart = input.substring(0, column);


        int indexOfSpaceForReplace = firstPart.lastIndexOf(" ");
        if (indexOfSpaceForReplace != -1) {
            firstPart = changeLastSpaceToNewLine(firstPart, indexOfSpaceForReplace);
        }

        return firstPart;
    }

    static private String changeLastSpaceToNewLine(String firstPart, int indexOfSpaceForReplace) {
        firstPart = firstPart.substring(0, indexOfSpaceForReplace) + "\n" + (indexOfSpaceForReplace - 1 == firstPart.length() ? "" : firstPart.substring(indexOfSpaceForReplace + 1));
        return firstPart;
    }

    static private String getNonChangeablePart(String input, int column) {
        String secondPart = "";
        if (input.length() != column) {
            secondPart = input.substring(column);
        }
        return secondPart;
    }


    public static void main(String[] args) {
        Wrapper.wrap("test break", 2);
    }
}
